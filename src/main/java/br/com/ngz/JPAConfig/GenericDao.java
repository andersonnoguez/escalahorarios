package br.com.ngz.JPAConfig;

import java.util.List;

/**
 *
 * @author anoguez
 * @param <PK> identificador
 * @param <T> entidade
 */
public interface GenericDao<PK, T> {

    public void save(T entity);

    public void update(T entity);

    public void delete(T entity);

    public T getById(PK pk);

    public List<T> findAll();

}
