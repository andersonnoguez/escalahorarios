package br.com.ngz.JPAConfig;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andersonNoguez
 * @param <PK> Identificador
 * @param <T> Entidade a ser passada
 */
@SuppressWarnings("unchecked")
public class GenericDaoImpl<PK, T> implements CrudService<PK, T> {

    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    public void save(T entity) {
        entityManager.persist(entity);
    }

    @Override
    public void update(T entity) {
        entityManager.merge(entity);
    }

    @Override
    public void delete(T entity) {
        entityManager.remove(entity);
    }

    @Override
    public T findById(PK pk) {
        return (T) entityManager.find(getTypeClass(), pk);
    }

    @Override
    public List<T> findAll() {
        return entityManager.createQuery(("FROM " + getTypeClass().getName()))
                .getResultList();
    }

    private Class<?> getTypeClass() {
        Class<?> clazz = (Class<?>) ((ParameterizedType) this.getClass()
                .getGenericSuperclass()).getActualTypeArguments()[1];
        return clazz;
    }

    @Override
    public void deleteByIds(List<T> entities) {
        if (entities != null && !entities.isEmpty()) {
            for (T entity : entities) {
                delete(entityManager.merge(entity));
            }
        }

    }

}

