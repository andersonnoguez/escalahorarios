package br.com.ngz.bean;

import br.com.ngz.model.Categoria;
import br.com.ngz.service.CategoriaService;
import br.com.ngz.utils.JSFUtils;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author andersonNoguez
 */
@ManagedBean
public class CategoriaBean implements BaseInterfaceBean, Serializable {

    private Categoria categoria;
    private List<Categoria> listaCategorias;
    private List<Categoria> selectedCategorias;
    private String descricao;
    private int ordemApresentacao;
    @Inject
    private CategoriaService categoriaService;

    @PostConstruct
    @Override
    public void init() {
        this.categoria = new Categoria();
        this.listaCategorias = categoriaService.findAll();
    }

    @Override
    public void store() {
        categoria.setDescricao(descricao);
        categoria.setOrdemApresentacao(1);

        try {
            categoriaService.save(categoria);
            JSFUtils.addMessage("categoriaCadastrada", categoria.getDescricao());
            resetFields();
        } catch (Exception e) {
            JSFUtils.addErrorMessage("categoriaNaoCadastrada", categoria.getDescricao());
        }
    }

    @Override
    public void deleteByIds() {
        try {
            if (selectedCategorias.isEmpty()) {
                JSFUtils.addMessage(FacesMessage.SEVERITY_WARN, "nenhumRegistroSelecionado", "");
                return;
            }

            categoriaService.deleteByIds(selectedCategorias);
            JSFUtils.addMessage("categoriaExcluida", "");
        } catch (Exception e) {
            JSFUtils.addErrorMessage("categoriaNaoExcluida", "");
        }
    }

    @Override
    public void resetFields() {
        this.categoria = new Categoria();
        this.descricao = "";
        this.ordemApresentacao = 0;
    }

    @Override
    public void onRowEdit(RowEditEvent event) throws Exception {
        try {
            categoriaService.update((Categoria) event.getObject());
            JSFUtils.addMessage("edicaoConcluida", ((Categoria) event.getObject()).getDescricao());
        } catch (Exception e) {
            JSFUtils.addErrorMessage("erroEdicao", ((Categoria) event.getObject()).getDescricao());
        }
    }

    @Override
    public void onRowCancel(RowEditEvent event) {
        JSFUtils.addMessage(FacesMessage.SEVERITY_WARN, "edicaoCancelada", ((Categoria) event.getObject()).getDescricao());
    }

    public List<Categoria> getListaCategorias() {
        return listaCategorias = categoriaService.findAll();
    }

    public void setListaCategorias(List<Categoria> listaCategorias) {
        this.listaCategorias = listaCategorias;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getOrdemApresentacao() {
        return ordemApresentacao;
    }

    public void setOrdemApresentacao(int ordemApresentacao) {
        this.ordemApresentacao = ordemApresentacao;
    }

    public List<Categoria> getSelectedCategorias() {
        return selectedCategorias;
    }

    public void setSelectedCategorias(List<Categoria> selectedCategorias) {
        this.selectedCategorias = selectedCategorias;
    }

}
