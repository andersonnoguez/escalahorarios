package br.com.ngz.bean;

import br.com.ngz.model.Categoria;
import br.com.ngz.model.Pessoa;
import br.com.ngz.model.PostoFuncao;
import br.com.ngz.service.CategoriaService;
import br.com.ngz.service.PessoaService;
import br.com.ngz.service.PostoFuncaoService;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author andersonNoguez
 */
@ManagedBean
public class EscalaBean implements BaseInterfaceBean, Serializable {

    private List<Categoria> listaCategorias;
    private List<PostoFuncao> listaPostoFuncao;
    private List<Pessoa> listaPessoa;
    private int idCategoria;
    @Inject
    private CategoriaService categoriaService;
    @Inject
    private PostoFuncaoService postoFuncaoService;
    @Inject
    private PessoaService pessoaService;

    @PostConstruct
    @Override
    public void init() {
        this.listaCategorias = categoriaService.findAll();

//        if (!listaCategorias.isEmpty()) {
//            this.idCategoria = listaCategorias.get(0).getIdCategoria();
//        }
        this.listaPessoa = pessoaService.findAll();
    }

    @Override
    public void store() {
        try {

        } catch (Exception e) {
        }
    }

    @Override
    public void deleteByIds() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void resetFields() {

    }

    @Override
    public void onRowEdit(RowEditEvent event) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onRowCancel(RowEditEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Categoria> getListaCategorias() {
        return listaCategorias;
    }

    public void setListaCategorias(List<Categoria> listaCategorias) {
        this.listaCategorias = listaCategorias;
    }

    public List<PostoFuncao> getListaPostoFuncao() {
        listaPostoFuncao = postoFuncaoService.findPostoFuncaoByCategoria(idCategoria);
        return listaPostoFuncao;
    }

    public void setListaPostoFuncao(List<PostoFuncao> listaPostoFuncao) {
        this.listaPostoFuncao = listaPostoFuncao;
    }

    public List<Pessoa> getListaPessoa() {
        return listaPessoa;
    }

    public void setListaPessoa(List<Pessoa> listaPessoa) {
        this.listaPessoa = listaPessoa;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

}
