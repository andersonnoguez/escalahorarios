package br.com.ngz.bean;

import br.com.ngz.model.Pessoa;
import br.com.ngz.service.LoginService;
import br.com.ngz.utils.SessionUtils;
import br.com.ngz.utils.MD5;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author anoguez
 */
@ManagedBean
public class LoginController implements Serializable{

    private String login;
    private String senha;
    @Inject
    private SessionUtils sessionUtils;
    @Inject
    private LoginService loginService;

    @PostConstruct
    public void init() {
        this.login = "";
        this.senha = "";
    }

    public void verificaLogin() {
        Pessoa pessoa = loginService.verificaLogin(login, MD5.setMD5(senha));

        if (pessoa != null) {
//            putPessoaOnSession(pessoa);
            redirectPage();
        } else {
            FacesContext.getCurrentInstance().addMessage("login",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuário ou senha incorretos", 
                            "Erro Login"));
        }

    }

    private void putPessoaOnSession(Pessoa pessoa) {
        sessionUtils.getSession().setAttribute("idPessoa", pessoa.getIdPessoa());
        sessionUtils.getSession().setAttribute("logado", pessoa.getLogin());
    }

    private void redirectPage() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().dispatch("./index");
        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

}
