package br.com.ngz.bean;

import br.com.ngz.model.Pessoa;
import br.com.ngz.service.PessoaService;
import br.com.ngz.utils.JSFUtils;
import br.com.ngz.utils.MD5;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author andersonNoguez
 */
@ManagedBean
public class PessoaBean implements BaseInterfaceBean, Serializable {

    private String nomePessoa;
    private String login;
    private String senha;
    private Boolean isAdmin;
    private Pessoa pessoa;
    private List<Pessoa> listaUsuarios;
    private List<Pessoa> selectedPessoas;
    @Inject
    private PessoaService pessoaService;

    @PostConstruct
    @Override
    public void init() {
        this.pessoa = new Pessoa();
        this.listaUsuarios = pessoaService.findAll();
    }

    @Override
    public void store() {

        pessoa.setNomePessoa(nomePessoa);
        pessoa.setLogin(login);
        pessoa.setSenha(MD5.setMD5(senha));
        pessoa.setIsAdmin(isAdmin);

        try {
            pessoaService.save(pessoa);
            JSFUtils.addMessage("usuarioCadastrado", pessoa.getNomePessoa());
            resetFields();
        } catch (Exception e) {
            JSFUtils.addErrorMessage("usuarioNaoCadastrado", pessoa.getNomePessoa());
        }
    }

    @Override
    public void deleteByIds() {
        try {

            if (selectedPessoas.isEmpty()) {
                JSFUtils.addMessage(FacesMessage.SEVERITY_WARN, "nenhumRegistroSelecionado", "");
                return;
            }

            pessoaService.deleteByIds(selectedPessoas);
            JSFUtils.addMessage("usuarioExcluido", "");
        } catch (Exception e) {
            JSFUtils.addErrorMessage("usuarioNaoExcluido", "");
        }
    }

    @Override
    public void resetFields() {
        nomePessoa = "";
        login = "";
        senha = "";
        isAdmin = Boolean.FALSE;
    }

    @Override
    public void onRowEdit(RowEditEvent event) {
        try {
            pessoaService.update((Pessoa) event.getObject());
            JSFUtils.addMessage("edicaoConcluida", ((Pessoa) event.getObject()).getNomePessoa());
        } catch (Exception e) {
            JSFUtils.addErrorMessage("erroEdicao", ((Pessoa) event.getObject()).getNomePessoa());
        }
    }

    @Override
    public void onRowCancel(RowEditEvent event) {
        JSFUtils.addMessage(FacesMessage.SEVERITY_WARN, "edicaoCancelada", ((Pessoa) event.getObject()).getNomePessoa());
    }

    public String getNomePessoa() {
        return nomePessoa;
    }

    public void setNomePessoa(String nomePessoa) {
        this.nomePessoa = nomePessoa;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public List<Pessoa> getListaUsuarios() {
        return listaUsuarios = pessoaService.findAll();
    }

    public void setListaUsuarios(List<Pessoa> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    public List<Pessoa> getSelectedPessoas() {
        return selectedPessoas;
    }

    public void setSelectedPessoas(List<Pessoa> selectedPessoas) {
        this.selectedPessoas = selectedPessoas;
    }

}
