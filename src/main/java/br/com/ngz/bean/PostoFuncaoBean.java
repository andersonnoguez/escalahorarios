package br.com.ngz.bean;

import br.com.ngz.model.Categoria;
import br.com.ngz.model.PostoFuncao;
import br.com.ngz.service.PostoFuncaoService;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author andersonNoguez
 */
@ManagedBean
public class PostoFuncaoBean implements BaseInterfaceBean, Serializable {

    private PostoFuncao postoFuncao;
    private String descricaoPosto;
    private int nrOrdem;
    private Categoria categoria;
    private List<PostoFuncao> listaPostoFuncao;
    @Inject
    private PostoFuncaoService postoFuncaoService;

    @PostConstruct
    @Override
    public void init() {
        this.postoFuncao = new PostoFuncao();
        this.listaPostoFuncao = postoFuncaoService.findAll();
    }

    @Override
    public void store() {
        postoFuncao.setDescricao(descricaoPosto);
        postoFuncao.setCategoria(categoria);//TODO
        postoFuncao.setOrdemApresentacao(1);//TODO arrumar para pegar dinamicamente
        postoFuncao.setCategoria(null);

        try {
            postoFuncaoService.save(postoFuncao);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Posto/Função Cadastrado", "ok"));
            resetFields();
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage("cad",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Posto/Função não pode ser cadastrado", "nok"));
        }

    }

    @Override
    public void deleteByIds() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void resetFields() {
        this.postoFuncao = new PostoFuncao();
        this.descricaoPosto = "";
        this.nrOrdem = 0;
        this.categoria = null;
    }

    @Override
    public void onRowEdit(RowEditEvent event) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onRowCancel(RowEditEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getDescricaoPosto() {
        return descricaoPosto;
    }

    public void setDescricaoPosto(String descricaoPosto) {
        this.descricaoPosto = descricaoPosto;
    }

    public int getNrOrdem() {
        return nrOrdem;
    }

    public void setNrOrdem(int nrOrdem) {
        this.nrOrdem = nrOrdem;
    }

    public List<PostoFuncao> getListaPostoFuncao() {
        return listaPostoFuncao;
    }

    public void setListaPostoFuncao(List<PostoFuncao> listaPostoFuncao) {
        this.listaPostoFuncao = listaPostoFuncao;
    }

}
