package br.com.ngz.bean;

import br.com.ngz.model.Categoria;
import br.com.ngz.model.TurnosHorarios;
import br.com.ngz.service.CategoriaService;
import br.com.ngz.service.TurnosHorariosService;
import java.io.Serializable;
import java.sql.Time;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author andersonNoguez
 */
@ManagedBean
public class TurnosBean implements BaseInterfaceBean, Serializable {

    private TurnosHorarios turnosHorarios;
    private Time horaInicial;
    private Time horaFinal;
    private int ordemTurno;
    private List<TurnosHorarios> listaTurnosHorarios;
    private List<Categoria> listaCategorias;
    @Inject
    private TurnosHorariosService turnosHorariosService;
    @Inject
    private CategoriaService categoriaService;

    @PostConstruct
    @Override
    public void init() {
        this.turnosHorarios = new TurnosHorarios();
        this.listaTurnosHorarios = turnosHorariosService.findAll();
        this.listaCategorias = categoriaService.findAll();
    }

    @Override
    public void store() {

        try {
            turnosHorarios.setOrdemTurno(ordemTurno);
            turnosHorarios.setHoraInicial(horaInicial);
            turnosHorarios.setHoraFinal(horaFinal);

        } catch (Exception e) {

        }

    }

    @Override
    public void deleteByIds() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void resetFields() {

    }

    @Override
    public void onRowEdit(RowEditEvent event) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onRowCancel(RowEditEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Time getHoraInicial() {
        return horaInicial;
    }

    public void setHoraInicial(Time horaInicial) {
        this.horaInicial = horaInicial;
    }

    public Time getHoraFinal() {
        return horaFinal;
    }

    public void setHoraFinal(Time horaFinal) {
        this.horaFinal = horaFinal;
    }

    public int getOrdemTurno() {
        return ordemTurno;
    }

    public void setOrdemTurno(int ordemTurno) {
        this.ordemTurno = ordemTurno;
    }

    public List<TurnosHorarios> getListaTurnosHorarios() {
        return listaTurnosHorarios;
    }

    public void setListaTurnosHorarios(List<TurnosHorarios> listaTurnosHorarios) {
        this.listaTurnosHorarios = listaTurnosHorarios;
    }

    public List<Categoria> getListaCategorias() {
        return listaCategorias;
    }

    public void setListaCategorias(List<Categoria> listaCategorias) {
        this.listaCategorias = listaCategorias;
    }

}
