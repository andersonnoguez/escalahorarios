package br.com.ngz.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author andersonNoguez
 */
@Controller
public class MainController {
    
    @RequestMapping("*/login")
    public String login(){
        return "login";
    }
    
    @RequestMapping("*/index")
    public String home(){
        return "index";
    }
    
}
