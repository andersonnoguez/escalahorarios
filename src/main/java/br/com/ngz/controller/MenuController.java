package br.com.ngz.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller responsável pelos acessos aos itens do menu
 *
 * @author andersonNoguez
 */
@Controller
public class MenuController {

    @RequestMapping("*/escalaCad")
    public String escalaCad() {
        return "moduloAdministrador/escala/escalaCad";
    }

    @RequestMapping("*/manterUsuarios")
    public String manterUsuarios() {
        return "moduloAdministrador/usuarios/manterUsuarios";
    }

    @RequestMapping("*/manterPostos")
    public String manterPostos() {
        return "moduloAdministrador/postosFuncoes/manterPostos";
    }

    @RequestMapping("*/manterCategorias")
    public String manterCategorias() {
        return "moduloAdministrador/categorias/manterCategorias";
    }

    @RequestMapping("*/manterTurnos")
    public String manterTurnos() {
        return "moduloAdministrador/turnos/manterTurnos";
    }

}
