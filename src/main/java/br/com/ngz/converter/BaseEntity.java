package br.com.ngz.converter;

/**
 *
 * @author Anderson Noguez
 */

public interface BaseEntity {
    
    public Long getId();  
    
}
