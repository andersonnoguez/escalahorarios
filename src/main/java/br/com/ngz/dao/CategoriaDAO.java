package br.com.ngz.dao;

import br.com.ngz.JPAConfig.GenericDaoImpl;
import br.com.ngz.model.Categoria;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author andersonNoguez
 */
@Stateless
public class CategoriaDAO extends GenericDaoImpl<Long, Categoria> {

    public List<Categoria> findByDescricao(String desc) {
        Query query = entityManager.createQuery(
                ("FROM " + Categoria.class.getName() + " c "
                + "WHERE c.descricao = :desc")
        );

        query.setParameter("desc", desc);

        return query.getResultList();
    }

}
