package br.com.ngz.dao;

import br.com.ngz.model.Pessoa;
import br.com.ngz.JPAConfig.GenericDaoImpl;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author anoguez
 */
@Stateless
public class LoginDAO extends GenericDaoImpl<Long, Object> {

    public List<Pessoa> verificaLogin(String login, String senha) {

        Query query = entityManager.createQuery(
                ("FROM " + Pessoa.class.getName() + " p "
                + "WHERE p.login = :login"
                + " and p.senha = :senha")
        );

        query.setParameter("login", login);
        query.setParameter("senha", senha);

        return query.getResultList();
    }

}
