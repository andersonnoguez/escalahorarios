package br.com.ngz.dao;

import br.com.ngz.model.Pessoa;
import br.com.ngz.JPAConfig.GenericDaoImpl;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author anoguez
 */
@Stateless
public class PessoaDAO extends GenericDaoImpl<Integer, Pessoa>{
    
    public List<Pessoa> findPessoasByName(String nome){
        Query query = entityManager.createQuery(
                ("FROM " + Pessoa.class.getName() + " p "
                + "WHERE p.nomePessoa = :nome")
        );

        query.setParameter("nome", nome);
        
        return query.getResultList();
    }
    
}
