package br.com.ngz.dao;

import br.com.ngz.JPAConfig.GenericDaoImpl;
import br.com.ngz.model.PostoFuncao;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author andersonNoguez
 */
@Stateless
public class PostoFuncaoDAO extends GenericDaoImpl<Long, PostoFuncao>{
    
    public List<PostoFuncao> findPostoFuncaoByCategoria(int idCategoria){
        Query query = entityManager.createQuery(
                ("FROM " + PostoFuncao.class.getName() + " p "
                + "WHERE p.categoria.idCategoria = :idCategoria")
        );

        query.setParameter("idCategoria", idCategoria);
        
        return query.getResultList();
    }
    
}
