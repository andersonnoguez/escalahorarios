package br.com.ngz.dao;

import br.com.ngz.JPAConfig.GenericDaoImpl;
import br.com.ngz.model.TurnosHorarios;
import javax.ejb.Stateless;

/**
 *
 * @author andersonNoguez
 */
@Stateless
public class TurnosHorariosDAO extends GenericDaoImpl<Long, TurnosHorarios>{
    
}
