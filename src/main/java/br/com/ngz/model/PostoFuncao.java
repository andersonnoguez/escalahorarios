package br.com.ngz.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author andersonNoguez
 */
@Entity
@Table(name = "POSTO_FUNCAO")
public class PostoFuncao implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idPostoFuncao;
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "ordemApresentacao")
    private int ordemApresentacao;

    @ManyToOne
    @JoinColumn(name = "idCategoria")
    private Categoria categoria;

    public int getIdPostoFuncao() {
        return idPostoFuncao;
    }

    public void setIdPostoFuncao(int idPostoFuncao) {
        this.idPostoFuncao = idPostoFuncao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getOrdemApresentacao() {
        return ordemApresentacao;
    }

    public void setOrdemApresentacao(int ordemApresentacao) {
        this.ordemApresentacao = ordemApresentacao;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

}
