package br.com.ngz.model;

import java.io.Serializable;
import java.sql.Time;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author andersonNoguez
 */
@Entity
@Table(name = "TURNOS_HORARIOS")
public class TurnosHorarios implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idTurnos;
    @Column(name = "ordemTurno")
    private int ordemTurno;
    @Column(name = "horaInicial")
    private Time horaInicial;
    @Column(name = "horaFinal")
    private Time horaFinal;

    public int getIdTurnos() {
        return idTurnos;
    }

    public void setIdTurnos(int idTurnos) {
        this.idTurnos = idTurnos;
    }

    public int getOrdemTurno() {
        return ordemTurno;
    }

    public void setOrdemTurno(int ordemTurno) {
        this.ordemTurno = ordemTurno;
    }

    public Time getHoraInicial() {
        return horaInicial;
    }

    public void setHoraInicial(Time horaInicial) {
        this.horaInicial = horaInicial;
    }

    public Time getHoraFinal() {
        return horaFinal;
    }

    public void setHoraFinal(Time horaFinal) {
        this.horaFinal = horaFinal;
    }

}
