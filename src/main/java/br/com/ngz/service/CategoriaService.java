package br.com.ngz.service;

import br.com.ngz.JPAConfig.BaseCrudService;
import br.com.ngz.dao.CategoriaDAO;
import br.com.ngz.model.Categoria;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author andersonNoguez
 */
@Stateless
public class CategoriaService extends BaseCrudService<Integer, Categoria>{

    @EJB
    private CategoriaDAO categoriaDAO;
    
    public CategoriaService() {
    }

    @Override
    public CategoriaDAO getDAO() {
       return categoriaDAO;
    }
    
    public List<Categoria> findByDescricao(String desc){
        return getDAO().findByDescricao(desc);
    }

    @Override
    public void deleteByIds(List<Categoria> list) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
