package br.com.ngz.service;

import br.com.ngz.model.Pessoa;
import br.com.ngz.dao.LoginDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author anoguez
 */
@Stateless
public class LoginService {
    
    @EJB
    private LoginDAO loginDAO;

    public LoginService() {
    }
    
    public Pessoa verificaLogin(String login, String senha){
        List result = loginDAO.verificaLogin(login, senha);
        
        if(!result.isEmpty()){
            return (Pessoa) result.get(0);
        }
        
        return null;
    }
    
}
