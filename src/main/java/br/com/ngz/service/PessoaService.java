package br.com.ngz.service;

import br.com.ngz.model.Pessoa;
import br.com.ngz.JPAConfig.BaseCrudService;
import br.com.ngz.dao.PessoaDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author anoguez
 */
@Stateless
public class PessoaService extends BaseCrudService<Integer, Pessoa> {

    @EJB
    private PessoaDAO pessoaDAO;
    
    public PessoaService() {
    }

    public List<Pessoa> findPessoasByName(String nome) {
        return getDAO().findPessoasByName(nome);
    }
    
    public List<Pessoa> findPessoasNotInEscala() {
        return getDAO().findAll();
    }

    @Override
    public PessoaDAO getDAO() {
        return pessoaDAO;
    }

}
