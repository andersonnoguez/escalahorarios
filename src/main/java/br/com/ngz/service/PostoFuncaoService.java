package br.com.ngz.service;

import br.com.ngz.dao.PostoFuncaoDAO;
import br.com.ngz.JPAConfig.BaseCrudService;
import br.com.ngz.model.PostoFuncao;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author andersonNoguez
 */
@Stateless
public class PostoFuncaoService extends BaseCrudService<Integer, PostoFuncao>{

    @EJB
    private PostoFuncaoDAO postoFuncaoDAO;

    public PostoFuncaoService() {
    }
    
    @Override
    public PostoFuncaoDAO getDAO() {
        return postoFuncaoDAO;
    }
    
    public List<PostoFuncao> findPostoFuncaoByCategoria(int idCategoria){
        return getDAO().findPostoFuncaoByCategoria(idCategoria);
    }

    @Override
    public void deleteByIds(List<PostoFuncao> list) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
