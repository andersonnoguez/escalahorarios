package br.com.ngz.service;

import br.com.ngz.JPAConfig.BaseCrudService;
import br.com.ngz.JPAConfig.GenericDaoImpl;
import br.com.ngz.dao.TurnosHorariosDAO;
import br.com.ngz.model.TurnosHorarios;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author andersonNoguez
 */
@Stateless
public class TurnosHorariosService extends BaseCrudService<Long, TurnosHorarios>{

    @EJB
    private TurnosHorariosDAO turnosHorariosDAO;

    public TurnosHorariosService() {
    }
    
    @Override
    public GenericDaoImpl getDAO() {
        return turnosHorariosDAO;
    }

    @Override
    public void deleteByIds(List<TurnosHorarios> list) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
