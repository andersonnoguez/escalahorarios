package br.com.ngz.tests;

import br.com.ngz.model.Categoria;
import br.com.ngz.service.CategoriaService;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author andersonNoguez
 */
public class CategoriaTest {
    
    private Categoria categoria;
    private CategoriaService categoriaService;
    
    @Before
    public void inicializaTestes() {
       this.categoria = new Categoria();
       this.categoriaService = new CategoriaService();
    }
    
//    @Test
    public void save(){
        categoria.setDescricao("Teste de inserção");
        categoria.setOrdemApresentacao(2);
        categoriaService.save(categoria);
    }
    
//    @Test
    public void delete(){
        List<Categoria> listaCategorias = categoriaService.findByDescricao("Teste de inserção");
 
        for (Categoria c : listaCategorias) {
            categoriaService.delete(c);
        }
   
        
        System.out.println("OK");
    }
    
}
