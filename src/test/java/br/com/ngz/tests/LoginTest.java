package br.com.ngz.tests;

import br.com.ngz.model.Pessoa;
import br.com.ngz.service.LoginService;
import br.com.ngz.utils.MD5;
import javax.inject.Inject;
import javax.transaction.Transactional;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author anoguez
 */
@Transactional
public class LoginTest {

//    @Inject
    private LoginService service;

    @Before
    public void inicializaTestes() {
    }

    public LoginTest() {
    }

    /**
     * Verifica se o login está ocorrendo com sucesso
     */
//    @Test
    public void loginsuccessful() {
        String login = "admin";
        String senha = "123";

        Pessoa p = service.verificaLogin(login, MD5.setMD5(senha));

        assertNotNull(p);
    }

    /**
     * Verifica o login em uma situação de erro nos dados de login
     */
//    @Test
//    public void loginError() {
//        String login = "testeErro";
//        String senha = "1239";
//
//        Pessoa p = loginService.verificaLogin(login, MD5.setMD5(senha));
//
//        assertNull(p);
//    }

}
