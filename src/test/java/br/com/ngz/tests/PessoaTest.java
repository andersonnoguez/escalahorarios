package br.com.ngz.tests;

import br.com.ngz.model.Pessoa;
import br.com.ngz.service.PessoaService;
import br.com.ngz.utils.MD5;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author anoguez
 */
public class PessoaTest {

    Pessoa pessoa;
    PessoaService pessoaService;

    @Before
    public void inicializaTestes() {
        pessoa = new Pessoa();
        pessoaService = new PessoaService();
    }

//    @Test
    public void savePessoa() {
        pessoa.setNomePessoa("usuarioTest3");
        pessoa.setSenha(MD5.setMD5("123"));
        pessoa.setLogin("usuarioTest");
        pessoa.setIsAdmin(Boolean.TRUE);
        
        pessoaService.save(pessoa);
    }

//    @Test
    public void listAll(){
        List<Pessoa> list = pessoaService.findAll();
        
        System.out.println("##########: " + list.size());
    }
    
    //@Test
//    public void deletePessoa() {
//        List<Pessoa> list = pessoaService.findPessoasByName("usuarioTest");
//
//        Assert.assertNotNull(list);
//
//        if (!list.isEmpty()) {
//            pessoaService.delete(list.get(1));
//        }
//
//    }
}
