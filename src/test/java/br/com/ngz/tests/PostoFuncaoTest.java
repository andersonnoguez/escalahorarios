package br.com.ngz.tests;

import br.com.ngz.model.PostoFuncao;
import br.com.ngz.service.PostoFuncaoService;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author andersonNoguez
 */
public class PostoFuncaoTest {
    
    PostoFuncao postoFuncao;
    PostoFuncaoService postoFuncaoService;
    
    @Before
    public void inicializaTestes() {
        this.postoFuncao = new PostoFuncao();
        this.postoFuncaoService = new PostoFuncaoService();
    }
    
    //@Test
    public void save(){
        postoFuncao.setDescricao("Teste de inserção posto/função");
        postoFuncao.setOrdemApresentacao(1);
        
        postoFuncaoService.save(postoFuncao);
    }
    
//    @Test
    public void findPostoFuncaoByCategoria(){
        List<PostoFuncao> lista = postoFuncaoService.findPostoFuncaoByCategoria(6);
        
        Assert.assertEquals(2, lista.size());
    }
    
}
